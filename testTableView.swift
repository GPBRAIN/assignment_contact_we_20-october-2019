//
//  testTableView.swift
//  Assignment_Contact_WE_20 October 2019
//
//  Created by The App Experts on 19/10/2019.
//  Copyright © 2019 The App Experts. All rights reserved.
//

import Foundation


class ContactTableViewModel

{
    //private var names : [String]
    var contractDataArray = [ObjectsContactsDataType] ()
    
    struct ObjectsContactsDataType
    {
        var contactName : String!
        var telephoneNumber : Int!
    }
       
//    private var header : [String]
//    private var footer : [String]
//
    
    init()
    {
  
        contractDataArray = [
            ObjectsContactsDataType(contactName:"Nelson Mandela", telephoneNumber: 776889977),
            ObjectsContactsDataType(contactName:"George Washington", telephoneNumber: 8990765678),
            ObjectsContactsDataType(contactName:"Mad Dog Mattis", telephoneNumber: 9666666666),
            ObjectsContactsDataType(contactName:"Alexander the Great", telephoneNumber: 55555888888),
            ObjectsContactsDataType(contactName:"Gengis Khan", telephoneNumber: 44444442344),
            ObjectsContactsDataType(contactName:"Mary Queen of Scots", telephoneNumber: 3333332345333),
            ObjectsContactsDataType(contactName:"Winston Churchil", telephoneNumber: 2222234522222),
            ObjectsContactsDataType(contactName:"Bernie Sanders", telephoneNumber: 111123451111),
            ObjectsContactsDataType(contactName:"Alexandria Ocasio-Cortez", telephoneNumber: 100010001000),
            ObjectsContactsDataType(contactName:"Ro Khanna", telephoneNumber: 776889977),
            ObjectsContactsDataType(contactName:"Nina Turner", telephoneNumber: 8990765678),
            ObjectsContactsDataType(contactName:"Julius Caesar", telephoneNumber: 9666666666),
            ObjectsContactsDataType(contactName:"Demis Hassabis", telephoneNumber: 55555888888),
            ObjectsContactsDataType(contactName:"Yann LeCun", telephoneNumber: 44444442344),
            ObjectsContactsDataType(contactName:"Andrew Ng", telephoneNumber: 3333332345333),
            ObjectsContactsDataType(contactName:"Stuart J Russell", telephoneNumber: 2222234522222),
            ObjectsContactsDataType(contactName:"Jurgen Schmidhuber", telephoneNumber: 111123451111),
            ObjectsContactsDataType(contactName:"Jean-Luc-Picard", telephoneNumber: 100010001000),
            ObjectsContactsDataType(contactName:"James T.kirk", telephoneNumber: 776889977),
           ObjectsContactsDataType(contactName:"Spock", telephoneNumber: 8990765678),
           ObjectsContactsDataType(contactName:"Data", telephoneNumber: 9666666666),
           ObjectsContactsDataType(contactName:"Leonard McCoy", telephoneNumber: 55555888888),
           ObjectsContactsDataType(contactName:"Benjamin Sisko", telephoneNumber: 44444442344),
           ObjectsContactsDataType(contactName:"Worf", telephoneNumber: 3333332345333),
           ObjectsContactsDataType(contactName:"Seven of Nine", telephoneNumber: 2222234522222),
           ObjectsContactsDataType(contactName:"Miles O'Brien", telephoneNumber: 111123451111),
           ObjectsContactsDataType(contactName:"Kathryn Janeway", telephoneNumber: 100010001000),
           ObjectsContactsDataType(contactName:"William Riker", telephoneNumber: 776889977),
           ObjectsContactsDataType(contactName:"Borg Queen", telephoneNumber: 8990765678),
           ObjectsContactsDataType(contactName:"Obi-Wan Kenobi", telephoneNumber: 9666666666),
           ObjectsContactsDataType(contactName:"Darth Vader", telephoneNumber: 55555888888),
           ObjectsContactsDataType(contactName:"Han Solo", telephoneNumber: 44444442344),
           ObjectsContactsDataType(contactName:"Luke Skywalker", telephoneNumber: 3333332345333),
           ObjectsContactsDataType(contactName:"Yoda", telephoneNumber: 2222234522222),
           ObjectsContactsDataType(contactName:"Chewbacca", telephoneNumber: 111123451111),
           ObjectsContactsDataType(contactName:"R2-D2", telephoneNumber: 100010001000),
           ObjectsContactsDataType(contactName:"Palpatine", telephoneNumber: 8990765678),
           ObjectsContactsDataType(contactName:"C-3PO", telephoneNumber: 9666666666),
           ObjectsContactsDataType(contactName:"Jabba the Hutt", telephoneNumber: 55555888888),
           ObjectsContactsDataType(contactName:"Count Dooku", telephoneNumber: 44444442344),
           ObjectsContactsDataType(contactName:"The Emperor", telephoneNumber: 3333332345333),
           ObjectsContactsDataType(contactName:"Jar Jar Blinks", telephoneNumber: 2222234522222),
           ObjectsContactsDataType(contactName:"Abraham Lincoln", telephoneNumber: 8990765678),
           ObjectsContactsDataType(contactName:"Franklin D. Roosevelt", telephoneNumber: 9666666666),
           ObjectsContactsDataType(contactName:"Theodore Roosevelt", telephoneNumber: 55555888888),
           ObjectsContactsDataType(contactName:"Napoleon Bonaparte", telephoneNumber: 44444442344),
           ObjectsContactsDataType(contactName:"Margaret Thatcher", telephoneNumber: 3333332345333),
           ObjectsContactsDataType(contactName:"Dwight D.Eisenhower", telephoneNumber: 2222234522222)
              
        ]

    }
}


extension ContactTableViewModel
{
    
    var section : Int
    {
        return 1
    }
    
    func numberOfRowsInSection (_ section : Int)-> Int
    {
        return contractDataArray.count
    }
    
    func object(at indexPath : IndexPath) -> Int?
    {
        if indexPath.section < 0 || indexPath.section > section
        {
            return nil
        }
        if indexPath.row < 0 || indexPath.row > numberOfRowsInSection(indexPath.section)
        {
            return nil
        }
     
        indexPath.section
        indexPath.row
        //ontractDataArray[indexPath.section].telephoneNumber[1]
        
        
        
        return contractDataArray[indexPath.section].telephoneNumber //.telephoneNumber[indexPath.row]  //got no idea why this is not working so just commented it out.
          
      //  return 1
             
           
           
    }  // END OF FUNCTION numberOfRowsInSection
    
}

